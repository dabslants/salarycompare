package com.qp.salary.compare;

import android.support.annotation.NonNull;

import java.text.DecimalFormat;

public class Period {

    static DecimalFormat decimalFormat = new DecimalFormat("#.##");
    static DecimalFormat intFormat = new DecimalFormat("#");

    @NonNull
    public static String format(double duration) {
        StringBuilder period = new StringBuilder();

        if (duration < 12) {
            // month
            period.append(monthToString(duration));
        } else {
            // years and months
            int year = (int) duration / 12;
            int month = (int) duration % 12;

            period.append(yearToString(year));

            if (month > 0) {
                period.append(" ");
                period.append(monthToString(month));
            }
        }
        return period.toString();
    }

    @NonNull
    public static float value(float duration) {
        // years and months
        float time = duration / 12;

        if (duration < 12) {
            // month
            return duration;
        }
        return time;
    }

    @NonNull
    public static String monthToString(double month) {
//        month = (month/10)* 12;
        String x = month > 1 ? "months" : "month";
        return intFormat.format(Math.ceil(month)) +
                " " +
                x;
    }

    @NonNull
    public static String yearToString(double year) {
        StringBuilder period = new StringBuilder();

//        double m = year % 12;

        String x = year > 1 ? "years" : "year";

        period.append(intFormat.format(year));
        period.append(" ");
        period.append(x);

//        if (m > 0) {
//            period.append(" ");
//            period.append(monthToString(m));
//        }

        return period.toString();
    }
}