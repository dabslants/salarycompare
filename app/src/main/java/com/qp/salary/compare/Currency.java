package com.qp.salary.compare;
import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Currency extends AsyncTask<String, Void, Double> {

    @Override
    protected Double doInBackground(String... curr) {
        HashMap<String, String> details = new HashMap<>();
        double rate = 0;
        try {
            Document doc = Jsoup.connect("https://www.msn.com/en-us/money/currencydetails/fi-NGN" + curr).get();

            Elements info = doc.select(".truncated-string");
            Element key = null;
            for (int i = 0; i< info.size(); i++) {
                if (i%2 == 0)
                    key = info.get(i);
                else
                    details.put(key.text(), info.get(i).text());
            }
            for (Map.Entry<String, String> t : details.entrySet()) {
                System.out.printf("%s: %s%n", t.getKey(), t.getValue());
            }

            rate = Double.parseDouble(details.get("Open"));

        } catch (IOException e) {
//            e.printStackTrace();
        }
        return rate;
    }

}
