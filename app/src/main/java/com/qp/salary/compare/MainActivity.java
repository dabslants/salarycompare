package com.qp.salary.compare;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    final int senatorSalary = 13500000;
    final int minimumWage = 18000;
    static String currency;
    static int currencyKey;
    static double rate;
    static HashMap<String, String> currencies;
    static DecimalFormat format = new DecimalFormat("#.##");
    static TextView naira;
    static TextView dollar;
    static TextView pound;
    static TextView euro;
    static EditText entry;
    static TextView heading;
    // Check if no view has focus:
    View view = this.getCurrentFocus();

    public static void start(Context c) {
        Intent intent = new Intent(c, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        c.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        naira = findViewById(R.id.naira);
        dollar = findViewById(R.id.dollar);
        pound = findViewById(R.id.pound);
        euro = findViewById(R.id.euro);
        entry = findViewById(R.id.entry);
        Button submit = findViewById(R.id.submit);
        final TextView result = findViewById(R.id.result);

//        HorizontalBarChart chart = findViewById(R.id.chart);
//
//        BarData data = new BarData(getDataSet());
//        chart.setData(data);
//        chart.animateXY(2000, 2000);
//        chart.invalidate();
        // add a lot of colors

//        PieChart pieChart = findViewById(R.id.chart);
//        PieData data = new PieData(getPieDataSet());
//        pieChart.setData(data);
//        Description s = new Description();
//        s.setText("Number of months you require to earn #13,500,000");
//        pieChart.setDescription(s);
//        pieChart.invalidate(); // refresh

        // currency
        currencies = new HashMap<String, String>();
        currencies.put(getString(R.string.naira), "NGN");
        currencies.put(getString(R.string.dollar), "USD");
        currencies.put(getString(R.string.pound), "GBP");
        currencies.put(getString(R.string.euro), "EUR");

        // default
        currencyKey = R.string.naira;
        currency = "NGN";

        setCurrency();
        reEvaluate();

//        TextView naira = findViewById(R.id.naira);
//        setCurrency(naira, R.string.naira);
//        TextView dollar = findViewById(R.id.dollar);
//        setCurrency(dollar, R.string.dollar);
//        TextView pound = findViewById(R.id.pound);
//        setCurrency(pound, R.string.pound);
//        TextView euro = findViewById(R.id.euro);
//        setCurrency(euro, R.string.euro);


//        final double convertedSal = senatorSalary * rate;


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String input = entry.getText().toString().trim();
                if (input.isEmpty()) {
                    input = format.format(minimumWage * rate);
                }
                try {
                    float monthEarnings = Float.parseFloat(input) /
                            (float) rate;
//                    Log.e("SSSSSSSSS", ""+monthEarnings);
                    StringBuilder r = new StringBuilder();
//                        r.append(String.format("It would take you %s to earn a Nigerian senator's monthly allowance (\u20A613 500 000)", _format_time(senatorSalary / monthEarnings)));
//                        r.append("\n\nA senator's income (including expenses) is about \u20A6162 000 000 per year.");
//                        r.append(String.format("\n\nIt would take you %s to earn the total yearly allowance of a Nigerian senator", _format_time((senatorSalary * 12) / monthEarnings)));
//                        result.setText(String.format("It would take you %s to earn same as a Nigerian senator's monthly allowance (\u20A613 500 000)", _format_time(senatorSalary / monthEarnings)));
//                        result.setText(String.format("\n\nIt would take you %s to earn same as a Nigerian senator's monthly allowance (\u20A613 500 000)", _format_time((senatorSalary * 12) / monthEarnings)));
//                        result.setText(r.toString());

                    MonthChartActivity.start(MainActivity.this, monthEarnings);


                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Invalid input.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setCurrency() {

        naira.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency = currencies.get(getString(R.string.naira));
                currencyKey = R.string.naira;

                naira.setBackgroundColor(getResources().getColor(R.color.colorPink));
                dollar.setBackgroundColor(R.drawable.rectangle);
                pound.setBackgroundColor(R.drawable.rectangle);
                euro.setBackgroundColor(R.drawable.rectangle);

                reEvaluate();
            }
        });

        dollar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency = currencies.get(getString(R.string.dollar));
                currencyKey = R.string.dollar;

                dollar.setBackgroundColor(getResources().getColor(R.color.colorPink));
                naira.setBackgroundColor(R.drawable.rectangle);
                pound.setBackgroundColor(R.drawable.rectangle);
                euro.setBackgroundColor(R.drawable.rectangle);

                reEvaluate();
            }
        });

        pound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency = currencies.get(getString(R.string.pound));
                currencyKey = R.string.pound;

                pound.setBackgroundColor(getResources().getColor(R.color.colorPink));
                dollar.setBackgroundColor(R.drawable.rectangle);
                naira.setBackgroundColor(R.drawable.rectangle);
                euro.setBackgroundColor(R.drawable.rectangle);

                reEvaluate();
            }
        });

        euro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency = currencies.get(getString(R.string.euro));
                currencyKey = R.string.euro;

                euro.setBackgroundColor(getResources().getColor(R.color.colorPink));
                dollar.setBackgroundColor(R.drawable.rectangle);
                pound.setBackgroundColor(R.drawable.rectangle);
                naira.setBackgroundColor(R.drawable.rectangle);

                reEvaluate();
            }
        });
    }

    private void reEvaluate() {
        try {
            rate = currency == "NGN" ? 1 : new Currency().execute(currency).get();
            heading = findViewById(R.id.heading);
            heading.setText(String.format(getString(R.string.tagB), getString(currencyKey), format.format(senatorSalary * rate)));

            entry.setHint(String.format("%s%s", getString(currencyKey), format.format(18000 * rate)));
//            Log.e("SSSSSSSSS", ""+rate);
        } catch (InterruptedException e) {
//            e.printStackTrace();
        } catch (ExecutionException e) {
//            e.printStackTrace();
        }
    }

    private void setChart() {
        PieChart pieChart = findViewById(R.id.chart);
        PieData data = new PieData(getPieDataSet());
        pieChart.setData(data);
        Description s = new Description();
        s.setText("Number of months you require to earn #13,500,000");
        pieChart.setDescription(s);
        pieChart.invalidate(); // refresh
    }

    private BarDataSet getDataSet() {

        ArrayList<BarEntry> entries = new ArrayList();
        entries.add(new BarEntry(1f, 1));
        entries.add(new BarEntry(2f, 750));

        BarDataSet dataset = new BarDataSet(entries, "hi");
        return dataset;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> labels = new ArrayList();
        labels.add("You");
        labels.add("A Senator");
        return labels;
    }

    private PieDataSet getPieDataSet() {
        List<PieEntry> entries = new ArrayList<>();

        entries.add(new PieEntry(750f, "You"));
        entries.add(new PieEntry(1f, "Senator"));

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());


        PieDataSet set = new PieDataSet(entries, "Months needed");
        set.setColors(colors);
        return set;
    }

    private class Currency extends AsyncTask<String, Void, Double> {

        @Override
        protected Double doInBackground(String... curr) {
//            Log.e("DDDDDDDDDD", );
            HashMap<String, String> details = new HashMap<>();
            double rate = 0;
            try {
                Document doc = Jsoup.connect("https://www.msn.com/en-us/money/currencydetails/fi-NGN" + curr[0]).get();

                Elements info = doc.select(".truncated-string");
                Element key = null;
                for (int i = 0; i< info.size(); i++) {
                    if (i%2 == 0)
                        key = info.get(i);
                    else
                        details.put(key.text(), info.get(i).text());
                }
                for (Map.Entry<String, String> t : details.entrySet()) {
                    System.out.printf("%s: %s%n", t.getKey(), t.getValue());
                }

                rate = Double.parseDouble(details.get("Open"));

            } catch (IOException e) {
//            e.printStackTrace();
            }
            return rate;
        }
    }

}
